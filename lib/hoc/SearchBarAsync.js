import React, {
  Component,
  Fragment
} from 'react'
import { AsyncTypeahead } from 'react-bootstrap-typeahead'
import _ from 'lodash'
import { InputGroup, InputGroupAddon, InputGroupText, Input } from 'reactstrap';



class SearchBar extends Component {
  state = {
    isLoading: false,
    options: [],
  }

  _handleChange = e => {
    const { checked, name } = e.target
    this.setState({ [name]: checked })
  }

  _handleSearch = query => {
    const {
      args,
    } = this.props
    const {
      handleSearch,
    } = args
    const { handleRequest } = this.props
    this.setState({ isLoading: true })
    handleSearch(query)
      .then(result => {

        this.setState({
          isLoading: false,
          options:result,
        })
      })
  }
  render() {
    const {
      args,
    } = this.props
    const {
      labelKey
    } = args
    return (
      <Fragment>
        <InputGroup>
          <InputGroupAddon addonType="prepend">@</InputGroupAddon>
          <AsyncTypeahead
          {...this.state}
          labelKey={labelKey}
          minLength={3}
          clearButton
          promptText={'Buscando...'}
          searchText={'Buscando...'}
          onSearch={this._handleSearch}
        />
        </InputGroup>
      </Fragment>
    ) 
  }
}

export default SearchBar