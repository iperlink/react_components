import React, {
  Component,
  Fragment,
} from 'react'
import { Typeahead } from 'react-bootstrap-typeahead'
import _ from 'lodash'
import { InputGroup, InputGroupAddon, InputGroupText, Button } from 'reactstrap'



class SearchBar extends Component {
  state = {
    isLoading: false,
    options: [],
  }

  _handleSearch = query => {
    const {
      args,
    } = this.props
    const {
      handleSearch,
    } = args
    const { handleRequest } = this.props
    this.setState({ isLoading: true })
    handleSearch(query)
      .then(result => {
        this.setState({
          isLoading: false,
          options: result,
        })
      })
  }
  render() {
    const {
      args,
    } = this.props
    const {
      labelKey,
      options,
    } = args
    return (
      <Fragment>
        <InputGroup>
          <Typeahead
          {...this.state}
          labelKey={labelKey}
          options={options}
          minLength={3}
          clearButton
        />
          <Button bsStyle="primary" type="submit">
            Buscar
          </Button>
        </InputGroup>
      </Fragment>
    )
  }
}

export default SearchBar
