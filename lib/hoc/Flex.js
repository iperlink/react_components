import React from 'react'
import _ from 'lodash'

const Flex = p => {
  const {
    store,
    args,
    flexArray,
    listArray,
    ConnFormArray,
  } = p
  const L = flexArray[args.flexName]
  const {
    rows,
  } = L
  const {
    filtered,
    filterKeys,
    dateRangeFilter,
    dateRangeFilterKey,
  } = args
  const componentArray = p.components
  const retout = _.map(rows, components => {
    const ret = _.map(components, x => {
      const Component = p.components[x.component]
      const { width, args } = x
      const result = (<div
        style={{
          flexGrow: width,
          }}>
          <Component
            {...p}
            {...componentArray}
            listArray={listArray}
            filtered={filtered}
            filterKeys={filterKeys}
            dateRangeFilter={dateRangeFilter}
            dateRangeFilterKey={dateRangeFilterKey}
            width={width}
            ConnFormArray={ConnFormArray}
            flexArray={flexArray}
            store={store}
            args={args}
        />

        </div>)
      return result
    })
    const display = components[0].display || 'block'
    return <div
        className='flexOut'
        style={{
          display,
          flexDirection: 'row',
        }}
        >{ret}
      </div>
  })

  return <div>{retout}
  </div>
}

export default Flex
