import React from 'react'
import { getDataFromQuery } from '../utils'
import BreadcrumbComp from '../components/BreadcrumComp'
import Spinner from '../components/Spinner.component'

const Loader = props => {
  const {
    args,
    components,
    localState,
    mainData,
    match,
    switchEfect,
    client,
    routeTitle,
  } = props
  const {
    filterKeys,
    success,
    filtered,
    dateRangeFilter,
    dateRangeFilterKey,
  } = args
  let ret
  if (mainData && mainData.loading) {
    return <Spinner name='double-bounce' />
  }
  if (mainData && mainData.error) {
    ret = <div>ha ocurrido un error</div>
    return <div>
      <BreadcrumbComp
        args={args}
        title={routeTitle}

        match={match}
      />
      {ret}
    </div>
  }
  console.log(components)
  const Comp = components[success]
  if (!Comp) {
    const compNames = _.values(_.mapValues(components, (comp, key) => <li>{key}</li>))
    return <div>
      el componente {success} no esta construido
      <span>componentes de la aplicacion</span>
      <ul>{compNames}</ul>

    </div>
  }
  console.log(props)
  const {
    data,
    dataParams,
    extras,
    head,
    name,
    title,
    aditional,
  } = getDataFromQuery(props)
  ret = <div>
    <Comp
      data={data}
      aditional={aditional}
      components={components}
      dataParams={dataParams}
      dateRangeFilter={dateRangeFilter}
      dateRangeFilterKey={dateRangeFilterKey}
      extras={extras}
      filtered={filtered}
      filterKeys={filterKeys}
      head={head}
      localState={localState}
      name={name}
      switchEfect={switchEfect}
      title={title}
      client={client}
    />
  </div>
  return <div>
    <BreadcrumbComp
      data={data}
      title={routeTitle}
    />
    {ret}
  </div>
}

export default Loader
//#f5f5f5