import React, { PureComponent } from 'react'
import _ from 'lodash'
import Table from './Table'
import Btn from './Btn'
import Form from './Form'

const Row = (p) => {
  const {
    store,
    args,
    flexArray,
    listArray,
    ConnFormArray,
  } = p
  const L = flexArray[args.flexName]
  const {
    rows
  } = L
  const componentArray = p.components
  const retout = _.map(rows, components => {
    const ret = _.map(components, x => {
      const Component=p.components[x.component]
      const { width, args } = x
      const result = (<div
        style={{
          flexGrow: width,
          borderColor:'#000',
          border:'solid'
          }}>
          <Component
            {...p}
            {...componentArray}
            listArray={listArray}
            width={width} 
            ConnFormArray={ConnFormArray}
            flexArray={flexArray}
            store={store}
            args={args}
        />
        
        </div>)
      return result
    })

    return <div 
        className='flexOut'
        style={{
          display: 'flex',
          flexDirection: 'row',
        }}
        >{ret}
      </div>
  }) 

  return <div>{retout}
  </div>
}

export default Row