export const menuStyle = {
  bmBurgerButton: {
    position: 'fixed',
    width: '36px',
    height: '30px',
    left: '20px',
    top: '20px',
  },
  bmBurgerBars: {
    background: '#373a47',
  },
  bmCrossButton: {
    height: '24px',
    width: '24px',
  },
  bmCross: {
    background: '#bdc3c7',
  },
  bmMenu: {
   // background: '#373a47',
    background:'linear-gradient(#373a47, #373a47)',
    paddingTop: '20px',
    fontSize: '1.15em',
  },
  bmMorphShape: {
    fill: '#373a47',
  },
  bmItemList: {

  },
  bmOverlay: {
    background: 'rgba(0, 0, 0, 0.3)',
  },
}
