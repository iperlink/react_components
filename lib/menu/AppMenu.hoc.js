import React, { Component } from 'react'
import { slide as Menu } from 'react-burger-menu'

import dropDown from './dropdownMenu.component'
import { menuStyle } from './menu.css'

export default class AppMenu extends Component {
  state = {
    open: false,
  }

  isMenuOpen = () => this.state.open

  render() {
    const menu = dropDown(this.props)


    const ret = <Menu
      styles={menuStyle}
      width={380}
      pageWrapId={'page-wrap'}
      isOpen={this.state.open}
      onStateChange={this.isMenuOpen}
      outerContainerId={'outer-container'}
    ><div className="navigationLogo" />
      <div className="navigation">
        {menu}

      </div>
    </Menu>
    return (ret)
  }
}
