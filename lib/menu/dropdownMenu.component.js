import React from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'

const mapPrivileges = (x, privilegesArr) => {
  const currPrivileges = x.privileges
  const comparedPriv = _.compact(_.map(currPrivileges, P => {
    if (privilegesArr.includes(P)) {
      return P
    }
  }))
  if (comparedPriv.length === 0) {
    return null
  }
  const li = <li
    key={`liMenu${x.path}`}
    ><Link
      to={x.path}
    >
      {x.link}
    </Link>
    </li>
  return li
}

const dropDown = props => {
  const {
    routeArr,
    privileges,
  } = props
  const grouped = _.groupBy(routeArr.childs, 'group')
  const menu = _.values(_.mapValues(grouped, (group, key) => {
    if (key === 'hidden') {
      return null
    }
    const privilegesArr = privileges.split(',')
    const content = _.compact(_.map(group, x => mapPrivileges(x, privilegesArr)))

    return <ul>
      <li
      className="has-sub"
      key={`liMenuCont${key}`}
      >
      <a href="#">{key}</a>
      </li>
        <ul>{content}</ul>
      </ul>
  }))
  return menu
}

export default dropDown
