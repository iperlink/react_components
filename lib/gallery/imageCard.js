
import React from 'react'
import {
  Card,
  CardImg,
} from 'reactstrap'

const Example = props => {
  const {
    thumb,
    clickHandler,
    onChange,
    url,
  } = props
  return (
    <div
      style={{
        flexGrow: 1,
        width: '400px',
        height: '250px',
        padding: '10px',
      }}
    >
      <Card style={{
        border: 'none',
        backgroundColor: '#f5f5f5',
      }} >
        <CardImg onClick={() => clickHandler(thumb)}
          top width="100%"
          src={thumb}
          alt="Card image cap"
        />
        <input
          type='checkbox'
          onChange={() => onChange(url)}
        ></input>
      </Card>

    </div>
  )
}

export default Example

