import React, { Component } from 'react'

import GalleryComponent from './gallery.component'
import FilterWraper from '../filters/FilterWrapper'

class GalleryRow extends Component {
  state = {
    selected: [],
  }

  render() {
    const {
      data,
      dataParams,
      name,
      title,
      filtered = false,
      filterKeys,
      dateRangeFilter,
      dateRangeFilterKey,
    } = this.props
    if (data.length === 0) {
      return <div>No existen datos</div>
    }
    const params = {
      data,
      dataParams,
      name,
      title,
    }
    const ret = GalleryComponent(params)
    if (filtered) {
      return <FilterWraper
        {...params}
        filterKeys={filterKeys}
        dateRangeFilter={dateRangeFilter}
        dateRangeFilterKey={dateRangeFilterKey}
        ComponentName={GalleryComponent}
      />
    }
    return (<div style={{ flex: 1 }} id={name}>
      {ret}
    </div>)
  }
}

export default GalleryRow
