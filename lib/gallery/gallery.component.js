import React, { Component } from 'react'
import _ from 'lodash'

import { Container, Row, Col, Modal, CardImg, Button } from 'reactstrap'
import FaOut from 'react-icons/lib/fa/arrow-down'
import moment from 'moment'

import ImageCard from './imageCard'

const showData = (Data, Title, text, clickHandler, selectHandler) => {
  const cards = _.map(Data, d => {
    const SRCs = d.images.split(',')
    const links = _.compact(_.map(SRCs, SRC => {
      const thumb = `https://s3.amazonaws.com/secdroneimages/${SRC}`
      const component = <ImageCard
        clickHandler={clickHandler}
        title={d.objective}
        subTitle={moment(d.date).add(4, 'hours').format('D MMM YYYY')}
        url={SRC}
        thumb={thumb}
        onChange={selectHandler}
        text={d[text]}
        style={{
          width: '200px',
        }
        }
      />

      return component
    }))
    return links
  })
  const chunked = _.chunk(_.flatten(cards), 3)
  const rows = _.map(chunked, ch => {
    const cols = _.map(ch, c => <Col xs="4" >{c}</Col>)
    const result = <Container
      className={'imageRowContainer'}
    >
      <Row
        className='imageRow'
      >
        {cols}
      </Row>
    </Container>
    return result
  })

  return (
    <div>
      {rows}
    </div>

  )
}

class CardRow extends Component {
  state = {
    visible: false,
    selected: [],
  }

  sendDownload = () => {
    const apiAdr = (process.env.NODE_ENV === 'production')
      ? 'https://api.wason.cloud/download'
      : 'http://localhost:4000/download'

    const url = apiAdr
    const string = this.state.selected.join()
    return post(url, formData)
  }
  handleSelect = url => {
    if (this.state.selected.includes(url)) {
      const newState = _.without(this.state.selected, url)
      this.setState({
        selected: newState,
      })
    } else {
      const newState = this.state.selected.concat([url])
      this.setState({
        selected: newState,
      })
    }
  }
  clickHandler = img => {
    this.setState({
      visible: img,
    })
  }
  close = img => {
    this.setState({
      visible: false,
    })
  }
  render() {
    const {
      data,
      dataParams,
    } = this.props
    const { selected } = this.state
    const {
      text,
    } = dataParams
    const { visible } = this.state
    if (data.length === 0) {
      return null
    }
    const ret = showData(data, data[0].name, text, this.clickHandler, this.handleSelect)
    const apiAdr = (process.env.NODE_ENV === 'production')
      ? 'https://api.wason.cloud/download'
      : 'http://localhost:4000/download'

    const string = this.state.selected.join()
    const path = `${apiAdr}?files=${string}`
    return <div>

      <Modal
        isOpen={visible}
        toggle={this.toggle}
        centered={true}
        size={'lg'}
      >
        <CardImg onClick={this.close}
          top width="100%"
          src={visible}
          alt="Card image cap"
        />
      </Modal>
      <div>Seleccionados {selected.length}

        <a
          href={path}
        >
          <Button
            key='activatedInTable'
            id={'logoutBtn'}
            color="primary"
          >
            <FaOut />
          </Button>
        </a>

      </div>
      {ret}
    </div>
  }
}

export default CardRow
