import React, { PureComponent } from 'react'
import { Toggle } from 'office-ui-fabric-react/lib/Toggle'

class Switch extends PureComponent {
  render() {
    const {
      activated,
      text,
      withText,
      switchData,
      fn,
    } = this.props
    const ret = withText
      ?
        <Toggle
          checked={activated}
          label={text}
          onChanged={ () => fn(switchData) }
      />
      : <Toggle
        checked={activated}
        onChanged={ () => fn(switchData) }
    />
    return <div>{ret}</div>
  }
}

export default Switch