import React from 'react'
import { Card, CardImg, CardText, CardBody, CardTitle, CardSubtitle, Button } from 'reactstrap'

import { parseJ, copyStringToClipboard } from '../utils'

const share = (link) => {
  copyStringToClipboard(`http://public.wason.cloud/?id=${link}`)
  alert('vinculo copiado al portapapeles')
}

const Example = props => {
  const {
    title, subTitle, text, url,
  } = props
  const parsedUrl = parseJ(url)
  const { link } = parsedUrl
  return (
    <div
      style={{
        flexGrow: 1,
        width: '350px',
        height: '400px',
        padding: '10px',
      }}
    >
      <Card
        style={{
          border: 'none',
        }}
      >
        <span
          key={`span${link}`}
          className={`wistia_vid wistia_embed wistia_async_${link} popover=true popoverAnimateThumbnail=true`}
          style={{
            display: 'inline-block',
            height: '214px',
            position: 'relative',
            width: '330px',
          }}
        >
          &nbsp;
        </span>
        <CardBody>
          <CardTitle>{title}</CardTitle>
          <CardSubtitle>{subTitle}</CardSubtitle>
          <CardText>{text}</CardText>
          <Button
            color="primary"
            className='filterBtn'
            onClick={() => share(link)}
          >
            Compartir
        </Button>
        </CardBody>
      </Card>
    </div>
  )
}

export default Example

// <a href="https://security-drone.wistia.com/medias/3uau9l6zd5"><img src="https://embed-ssl.wistia.com/deliveries/09f7a72a0f606a6335cf0ba561dd1c12a4c816ca.jpg?image_crop_resized=900x506&image_play_button=true&image_play_button_size=2x&image_play_button_color=54bbffe0" alt="09_05_18_PATIOS03" width="450" height="253" /></a>
