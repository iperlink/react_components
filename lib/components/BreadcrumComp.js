import React from 'react'


const BreadcrumbComp = props => {
  const {
    title,
    data,
    param,
  } = props
  
  const renderTitle= title || data[0].name
  return (
    <div className='titleBar'>
      {renderTitle}
    </div>
  )
}

export default BreadcrumbComp
