import React from 'react'
import Spinner from 'react-spinkit'

const styles = {
  cont: {
    width: '100%',
    height: '500px',
  },
  spinner: {
    marginLeft: 'auto',
    marginRight: 'auto',
    width: '60px',
    height: '60px',
    marginTop: '100px',
  }
}


const spinner = () => {
  const result = <div
    className={'spinnerCont'}
    style={styles.cont}
    >
  <Spinner
  name='double-bounce'
  style={styles.spinner}
  />
  </div>
  return result
}

export default spinner
