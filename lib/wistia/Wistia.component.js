import React from 'react'
import _ from 'lodash'
import { Button } from 'reactstrap'
const styles = {
  wraper: {
    position: 'relative',
  },
  swatch: {
    height: '100%',
    left: 0,
    opacity: 0,
    overflow: 'hidden',
    position: 'absolute',
    top: '0',
    //transition:'opacity 200ms',
    width: '100%',
  },
  img: {
    height: '100%',
    width: '100%'
  }
}
const Wistia = props => {
  const {
    src,
    clickHandler,
    location,
  } = props
  const {
    pathname,
  } = location

  const SRC = _.last(pathname.split('/'))
  const ret =
    <div>
      <div class={`wistia_embed wistia_async_${SRC} wistia_wraper`} style={styles.wraper}>
        <div class="wistia_swatch" style={styles.swatch} onClick={clickHandler}>
          <img
            src={`https://fast.wistia.com/embed/medias/${SRC}/swatch`}
            style={styles.img}
          />
        </div>
      </div>
      <Button

        onClick={() => clickHandler(false)}>Cerrar</Button>
    </div>
  return ret
}

export default Wistia

