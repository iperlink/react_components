import React from 'react'

import CardRowComponent from './CardRow.component'
import FilterWraper from '../filters/FilterWrapper'

const CardRow = props => {
  const {
    data,
    dataParams,
    name,
    title,
    filtered = false,
    filterKeys,
    dateRangeFilter,
    dateRangeFilterKey,
  } = props
  if (data.length === 0) {
    return <div>No existen datos</div>
  }
  const params = {
    data,
    dataParams,
    name,
    title,
  }

  if (filtered) {
    return (
      <FilterWraper
        {...params}
        filterKeys={filterKeys}
        dateRangeFilter={dateRangeFilter}
        dateRangeFilterKey={dateRangeFilterKey}
        ComponentName={CardRowComponent}
      />
    )
  }
  const ret = CardRowComponent(params)
  return (
    <div style={{ flex: 1 }} id={name}>
      {ret}
    </div>
  )
}

export default CardRow
