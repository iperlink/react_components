import React, { PureComponent } from 'react'
import _ from 'lodash'
import { Container, Row, Col } from 'reactstrap'
import moment from 'moment'

import Card from '../elements/Card'

const CardRow = props => {
  const {
    clickHandler, data, dataParams, title, grouped = true,
  } = props
  const showData = (Data, Title) => {
    const cards = _.compact(_.map(Data, d => {
      const { url } = d
      if (!url) {
        return null
      }
      const SRC = _.last(d.url.split('/'))
      const thumb = `https://fast.wistia.com/embed/medias/${SRC}/swatch`
      const component = (
        <Card
          title={d.objective}
          clickHandler={clickHandler}
          subTitle={d.name}
          url={SRC}
          thumb={thumb}
          text={moment(d.date)
            .add(4, 'hours')
            .format('D MMM YYYY') + ' ' + d.time}
        />
      )

      return component
    }))

    const chunked = _.chunk(cards, 3)
    const rows = _.map(chunked, ch => {
      const cols = _.map(ch, c => <Col xs="4">{c}</Col>)
      const result = (
        <Container className="videoRowContainer">
          <Row className={'videoRow'}>{cols}</Row>
        </Container>
      )
      return result
    })

    return (
      <div className={'videoCategory'}>
        <div className={'videoRowTitle'}> {Title}</div>
        {rows}
      </div>
    )
  }

  if (grouped) {
    const groupedData = _.groupBy(data, 'row')
    const results = _.map(groupedData, gr => {
      const ret = showData(gr, _.head(gr).row)
      return ret
    })

    return <div>{results}</div>
  }

  const ret = showData(data, title)
  return ret
}

export default CardRow
