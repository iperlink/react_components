import React, {
  Component,
  Fragment,
} from 'react'
import { Typeahead } from 'react-bootstrap-typeahead'
import _ from 'lodash'
import {
  Button,
  Form,
} from 'reactstrap'
import moment from 'moment'

import dateRangeFilterFn from './dateRange.filter'

const styles = {
  bar: {
    marginBottom: '25px',
  },
  form: {
    marginBottom: '25px',
    display: 'flex',
    justifyContent: 'flex-start',
  },
}
class SearchBar extends Component {
  state = {
    isLoading: false,
    hidden: false,
    options: [],
    filterValue: '',
    dateFrom: false,
    dateTo: false,

  }

  handleSubmit = () => {
    const {
      handleSubmitFn,
    } = this.props
    const {
      filterValue,
      dateFrom,
      dateTo,
    } = this.state
    this.setState({
      submited: true,
    }, () => {
      handleSubmitFn({
        filterValue,
        dateFrom,
        dateTo,
      })
    })
  }

  handleChangeDate = (e, type) => {
    const date = e.target.value
    this.setState({
      [type]: moment(date).add(4, 'hours'),
    })
  }

  handleFilterChange = query => {
    this.setState({
      filterValue: query['0'],
    })
  }

  show = () => {
    this.setState({
      hidden: !this.state.hidden,
    })
  }
  render() {
    const {
      args,
      dateRangeFilter = false,
    } = this.props
    const {
      labelKey,
      options,
    } = args
    const { hidden } = this.state
    const formComponentsDate = dateRangeFilter
      ? [dateRangeFilterFn({ handleChangeDate: this.handleChangeDate })]
      : []
    const formComponents = _.concat([
      <Button
        color="primary"
        className='filterBtn'
        onClick={this.handleSubmit}
      >
        Filtrar
    </Button>,
      <Button
        color="primary"
        className='hidefilterBtn filterBtn'
        onClick={this.show}
      >
        Ocultar
    </Button>],
      formComponentsDate,
      [<div style={{ width: '300px' }} >
        <Typeahead
          style={{ width: '480px' }}
          {...this.state}
          labelKey={labelKey}
          align='Align right'
          options={options}
          placeholder='Etiquetas'
          minLength={3}
          onChange={this.handleFilterChange}
          id={'filterTypeahead'}
        />
      </div>,

      ])
    const ret = hidden
      ? <div style={styles.form}>
        <Button
          color="primary"
          onClick={this.show}
        >
          Mostrar
        </Button>
      </div>
      : <div style={styles.form}>
        <Form
          inline
        >
          {formComponents}
        </Form>
      </div>
    return ret
  }
}

export default SearchBar
