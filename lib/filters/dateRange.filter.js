import React from 'react'
import _ from 'lodash'
import {
  InputGroup,
  FormGroup,
  Label,
  Input,
} from 'reactstrap'
import FaCalendar from 'react-icons/lib/fa/calendar'
import FaArrowRight from 'react-icons/lib/fa/arrow-right'


const dateRangeFilter = props => {
  const {
    handleChangeDate,
  } = props
  const result = <InputGroup className="mb-2 mr-sm-2 mb-sm-0">
    <FormGroup className="mb-2 mr-sm-2 mb-sm-0" key='filterFrom'>
      <Label for="filterFrom" className="mr-sm-2"><FaCalendar/></Label>
      <Input
        type="date"
        id="filterFrom"
        onChange={e => handleChangeDate(e, 'dateFrom')}
      />
    </FormGroup>
    <FaArrowRight className={'arrowRight'}/>
    <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
      <Label for="exampleDatetime" className="mr-sm-2"></Label>
      <Input
        type="date"
        id="FilterTo"
        onChange={e => handleChangeDate(e, 'dateTo')}
      />
      </FormGroup>
    </InputGroup>
  return result
}

export default dateRangeFilter
