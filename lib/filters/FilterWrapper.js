import React, {
  Component,
  Fragment,
} from 'react'
import _ from 'lodash'

import FilterBar from './FilterBar'
import {
  filterDateRange,
  getDataForFilter,
  filterFromValue,
} from './filtering.functions'

class FilterWraper extends Component {
  state = {
    data: [],
    argsForFilter: {},
    originalData: {},
  }

  componentWillMount() {
    const {
      data,
      filterKeys,
    } = this.props
    const filterData = getDataForFilter(data, filterKeys)
    const argsForFilter = {
      options: filterData,
      labelKey: option => `${option.value}`,
    }
    this.setState({
      argsForFilter,
      data,
      originalData: data,
    })
  }

  handleSubmitFilterChange = values => {
    const {
      filterValue,
      dateFrom,
      dateTo,
    } = values
    const {
      originalData,
    } = this.state
    const {
      dateRangeFilterKey,
    } = this.props

    const filteredDate = filterDateRange(
      originalData,
      dateRangeFilterKey,
      dateFrom,
      dateTo,
    )

    if (filterValue) {
      const filteredFromValue = filterFromValue(filteredDate, filterValue)
      this.setState({
        data: filteredFromValue,
      })
    } else {
      this.setState({
        data: filteredDate,
      })
    }
  }

  render() {
    const {
      data,
      argsForFilter,
    } = this.state
    const {
      extras,
      head,
      localState,
      name,
      ComponentName,
      dataParams,
      dateRangeFilter,
      switchEfect,
      title,
      toggleAll,
      toggleSingle,
      editFn,
      filtered,
      aditional,
    } = this.props

    return (
      <Fragment>
        <FilterBar
          handleSubmitFn={this.handleSubmitFilterChange}
          dateRangeFilter={dateRangeFilter}
          args={argsForFilter}
        />
        <ComponentName
          extras={extras}
          dataParams={dataParams}
          head={head}
          localState={localState}
          name={name}
          switchEfect={switchEfect}
          title={title}
          data={data}
          toggleAll={toggleAll}
          toggleSingle={toggleSingle}
          editFn={editFn}
          filtered={filtered}
          aditional={aditional}

        />
      </Fragment>
    )
  }
}

export default FilterWraper
