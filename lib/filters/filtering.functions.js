import _ from 'lodash'
import moment from 'moment'

const filterDate = (data, key, date, type) => {
  const result = date
    ? _.filter(data, x => {
      if (x[key]) {
        const xDate = moment(x[key]).add(4, 'hours')
        const belongs = type === '$gt'
          ? moment(x[key]).add(4, 'hours').isSame(date, 'day') || moment(x[key]).add(4, 'hours').isAfter(date, 'day')
          : moment(x[key]).add(4, 'hours').isSame(date, 'day') || moment(x[key]).add(4, 'hours').isBefore(date, 'day')
        return belongs
      }

      return x
    })
    : data
  return result
}

export const filterDateRange = (data, key, dateFrom, dateTo) => {
  const filteredFrom = dateFrom
    ? filterDate(data, key, dateFrom, '$gt')
    : data
  const filteredTo = dateTo
    ? filterDate(filteredFrom, key, dateTo, '$lt')
    : filteredFrom

  return filteredTo
}

const valuesFromKeys = (data, keys) => {
  const valuesFromDataUnFlatten = _.map(data, x => {
    const values = _.map(keys, key => {
      const result = { label: key, value: x[key] }
      return result
    })
    return values
  })
  const valuesFromData = _.uniqBy(_.flatten(valuesFromDataUnFlatten), 'value')

  return valuesFromData
}

const valuesFromTags = data => {
  const extracted = _.map(data, x => {
    if (x.tags) {
      const splitted = x.tags.split(',')
      return splitted
    }
    return null
  })
  const flatted = _.flatten(_.compact(extracted))
  const unique = _.uniq(flatted)
  const trimmed = _.map(unique, x => ({ label: 'tag', value: x }))
  return trimmed
}
export const getDataForFilter = (data, keys) => {
  const hasNoTags = !data[0].tags
  const fromkeys = valuesFromKeys(data, keys)
  if (hasNoTags) {
    return fromkeys
  }
  const fromTags = valuesFromTags(data)
  const concated = _.uniqBy(_.concat(fromkeys, fromTags), 'value')
  return concated
}

export const filterFromValue = (data, filterValue) => {
  const {
    label,
    value,
  } = filterValue
  if (label !== 'tag') {
    const result = _.filter(data, x => x[label] === value)
    return result
  }
  const result = _.filter(data, x => {
    if (x.tags) {
      const currTags = x.tags.split(',')
      return currTags.includes(value)
    }
    return false
  })
  return result
}
