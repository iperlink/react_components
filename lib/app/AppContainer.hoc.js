import React, { Component } from 'react'

import AppMenu from '../menu/AppMenu.hoc'
import { appStyle } from './app.css'
import {
  Button,
} from 'reactstrap'
import FaOut from 'react-icons/lib/fa/sign-out'
export default class AppContainer extends Component {
  logout = () => {
    sessionStorage.removeItem('userToken')
    location.reload()
  }

  render() {
    const {
      routeArr,
      Routes,
      privileges,
      curruser,
    } = this.props


    const ret = <div id="outer-container">
      <div className='userCard'>
        <Button
          color="success"
          className='userCardBtn'
        >{curruser.email}</Button><Button
          key='activatedInTable'
          id={'logoutBtn'}
          color="primary"
          onClick={this.logout}
        >
          <FaOut />
        </Button></div>
      <AppMenu
        routeArr={routeArr}
        privileges={privileges}
        curruser={curruser}
      />
      <div
        id="page-wrap"
        style={appStyle}
      >
        <Routes />
      </div>
    </div>
    return ret
  }
}
