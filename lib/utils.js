import _ from 'lodash'

export const getDataFromQuery = props => {
  const {
    args,
    dataSources,
    mainData,
  } = props
  const toOmit = ['match', 'location', 'history', 'staticContext', 'routeTitle', 'components', 'dataSources', 'flexArray', 'args', 'client', 'mainData', 'localState']

  const aditional = _.omit(props, toOmit)
  const {
    listName,
  } = args
  const list = dataSources[listName]
  const {
    dataParams,
    extras,
    head,
    mainData: { name },
    title,
    mappingFn,
    filterFn,
  } = list
  const rawData = mainData[name] || []
  const dataPreFilter = mappingFn
    ? _.map(rawData, x => mappingFn(x))
    : rawData
  const dataFiltered = filterFn
    ? filterFn(dataPreFilter)
    : dataPreFilter
  return {
    data: dataFiltered,
    dataParams,
    extras: extras || {},
    head,
    name: listName,
    title,
    aditional,
  }
}

export const toggleItemInArray = (collection, item) => {
  const index = _.indexOf(collection, item)
  if (index !== -1) {
    return _.without(collection, item)
  }
  return [...collection, item]
}


export const parseJ = data => {
  try {
    const parsed = JSON.parse(data)
    return parsed
  } catch (e) {

    return data
  }
}


export const copyStringToClipboard = (str) => {
  // Create new element
  var el = document.createElement('textarea');
  el.value = str;
  el.setAttribute('readonly', '');
  el.style = {position: 'absolute', left: '-9999px'};
  document.body.appendChild(el);
  // Select text inside element
  el.select();
  // Copy text to clipboard
  document.execCommand('copy');
  // Remove temporary element
  document.body.removeChild(el);
}