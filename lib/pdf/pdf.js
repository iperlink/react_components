import React, { Component } from 'react'
import { Document, Page } from 'react-pdf'
import { Button } from 'reactstrap'

export default class Pdf extends Component {
  state = {
    numPages: null,
    pageNumber: 1,
  }

  onDocumentLoad = ({ numPages }) => {
    this.setState({ numPages })
  }

  next = () => {
    this.setState({ pageNumber: this.state.pageNumber + 1 })
  }
  prev = () => {
    if (this.state.pageNumber > 1) {
      this.setState({ pageNumber: this.state.pageNumber - 1 })
    }
  }
  render() {
    const { pageNumber, numPages } = this.state
    const {
      data,
      dataParams,
    } = this.props
    const { pdf } = dataParams
    if (data.length === 0) {
      return null
    }
    const src = data[0][pdf]
    return (
      <div>
        <Document
          file={src}
          onLoadSuccess={this.onDocumentLoad}
        >
          <Page pageNumber={pageNumber} scale={2.0}/>

        </Document>
        <p>Page {pageNumber} of {numPages}</p>
        <Button
        onClick={this.prev}
        >{'Retroceder'}</Button>
        <Button
        onClick={this.next}
        >{'Avanzar'}</Button>
      </div>
    )
  }
}
