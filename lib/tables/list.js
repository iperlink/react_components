import React from 'react'
import { ListGroup, ListGroupItem } from 'reactstrap'
import _ from 'lodash'

export const list = (data, head) => {

  const items = _.map(data, x => <ListGroupItem>{x.name}</ListGroupItem>)
  const listItems = [
    <ListGroupItem>{_.head(head).title}</ListGroupItem>,
  ].concat(items)
  return (
    <div className='listDiv' >
      <ListGroup>
        {items}
      </ListGroup>
    </div>
  )
}

