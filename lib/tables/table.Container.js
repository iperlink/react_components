import React from 'react'
import { AutoSizer } from 'react-virtualized';
import { simpleTable } from './simpleTable.element'

const tableContainer = props => {
  const {
    data,
    editFn,
    extras,
    head,
    localState,
    name,
    switchEfect,
    title,
    toggleAll,
    toggleSingle,
    client,
    filtered,
  } = props
  const H = filtered
    ? '450px'
    : '500px'
  const ret = <div style={{height: H}} className={'tableDiv'}>
    <AutoSizer >
      {({ height, width }) => 
        {
        return simpleTable({
          data,
          editFn,
          extras,
          head,
          localState,
          name,
          switchEfect,
          title,
          toggleAll,
          toggleSingle,
          width,
          height,
          client,
        })
        }
      }
    </AutoSizer>
  </div>
  return ret
}

export default tableContainer