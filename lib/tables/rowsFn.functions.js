import _ from 'lodash'

export const editFn = elem => {

  console.log('called', elem)
}

export const toggleAll = (state, props) => {
  const {
    activated,
  } = state

  const {
    data,
    extras,
    switchEfect,
  } = props
  const {
    _checkboxFn,
  } = extras
  const current = activated.length
    ? []
    : _.map(data, (x, i) => i)
  const newActivated = current
  const mappedData = _.map(data, x => ({
    ...x,
    activated: newActivated,
  }))
  const result = {
    data: mappedData,
    activated: newActivated,
  }
  return result
}

export const toggleItemInArray = (collection, item) => {
  const index = _.indexOf(collection, item)
  if (index !== -1) {
    return _.without(collection, item)
  }
  return [...collection, item]
}
