import React, { Component } from 'react'
import {
  Button,
  Col,
  FormGroup,
  Label,
} from 'reactstrap'
import _ from 'lodash'
import { post } from 'axios'
import TextInput from '../forms/textInput.component'
import Select from '../forms/Select.component'
import TableContainer from './table.Container'
import FilterWraper from '../filters/FilterWrapper'
import DateSelectorContainer from '../forms/dateSelector.component'
import File from '../forms/file.component'
import { list } from './list'

import Switch from '../elements/Switch'
import {
  toggleAll,
  toggleItemInArray,
} from './rowsFn.functions'

const styles = {
  true: {
    backgroundColor: 'red',
  },
  false: {
    backgroundColor: 'green',
  },
}
const apiAdr = (process.env.NODE_ENV === 'production')
  ? 'https://api.wason.cloud/upload'
  : 'http://localhost:4000/upload'
export class Table extends Component {
  state = {
    activated: [],
    submited: false,
    editing: false,
    editingData: {},
    selectValues: {},
    uploading: false,
  }

  toggleAll = () => {
    const {
      extras,
    } = this.props
    const {
      _checkboxFn,
    } = extras
    const {
      fn,
    } = _checkboxFn
    const newState = toggleAll(this.state, this.props)
    const {
      activated,
      data,
    } = newState
    this.setState({
      activated,
    }, () => {
      fn({ allRows: data })
    })
  }

  toggleSingle = ({ rowData, rowIndex }) => {
    const {
      extras,
      localState,
      switchEfect,
    } = this.props
    const {
      _checkboxFn,
    } = extras
    const {
      fn,
    } = _checkboxFn
    const {
      activated,
    } = this.state
    const newActivated = toggleItemInArray(activated, rowIndex)
    this.setState({
      activated: newActivated,
    }, () => {
      fn({
        rowData, rowIndex, localState, switchEfect,
      })
    })
  }

  editFn = row => {
    this.setState({
      editing: true,
      editingData: row,
    })
  }
  fileUpload = file => {
    const url = apiAdr
    const formData = new FormData()
    formData.append('file', file)
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
      },
    }
    return post(url, formData, config)
  }
  handleEdit = async () => {
    const {
      client,
      extras,
    } = this.props
    const {
      editingData,
    } = this.state
    const {
      editParams,
    } = extras
    const uploaders = _.filter(editParams, { file: true })
    if (uploaders.length) {
      this.setState({
        uploading: true,
      })
      const promises = _.map(uploaders, uploader => {
        const file = document.getElementById(uploader.label).files[0]
        return this.fileUpload(file)
      })
      await Promise.all(promises)
      this.setState({
        uploading: false,
        result: 'Cargando...',
      })
    }
    const values = _.reduce(editParams, (acc, editParam) => {
      if (document.getElementById(editParam.label)) {
        acc[editParam.name] = document.getElementById(editParam.label).value
      }
      return acc
    }, {})
    const concated = {
      ...values,
      ...this.state.selectValues,
    }

    await extras._editFn(editingData._id, concated, client)
    this.setState({
      editing: false,
      editingData: {},
    })
  }

  handleChangeSelect = (param, value) => {
    const obj = {
      ...this.state.selectValues,
      [param]: value,
    }
    this.setState({ selectValues: obj })
  }
  handleSubmitSelected = () => {
    const {
      activated,
    } = this.state

    const {
      data,
      extras,
      client,
    } = this.props
    const {
      _checkboxFn,

    } = extras
    const {
      submit,
    } = _checkboxFn
    const selectedData = _.compact(_.map(data, (d, i) => {
      if (activated.includes(i)) {
        return d
      }
      return null
    }))

    this.setState({
      submited: true,
    }, () => {
      submit(selectedData, client)
    })
  }

  render() {
    const {
      data,
      dateRangeFilter,
      dateRangeFilterKey,
      extras,
      filterKeys,
      filtered = false,
      head,
      localState,
      name,
      switchEfect,
      title,
      aditional,

    } = this.props
    const {
      _checkboxFn,
      selector,
      editParams,
    } = extras
    const {
      activated,
      editing,
      editingData,
    } = this.state
    if (editing) {
      const editors = _.map(editParams, editParam => {
        if (editParam.select) {
          const optionsData = aditional[editParam.select][editParam.select]
          const options = _.map(optionsData, option => {
            const result = {
              label: option.name,
              value: option._id,
            }
            return result
          })
          return <FormGroup row>
            <Label for={'row'} sm={2}>{editParam.label}</Label>
            <Col sm={10}>
              <Select
                options={options}
                id={editParam.label}
                onChange={value => this.handleChangeSelect(editParam.name, value)}
              />
            </Col>
          </FormGroup>
        }
        if (editParam.file) {
          return <FormGroup row>
            <Label for={editParam.label} sm={2}>{editParam.label}</Label>
            <Col sm={10}>
              <File
                id={editParam.label}
              />
            </Col>
          </FormGroup>
        }
        if (editParam.date) {
          return <FormGroup row>
            <Label for={'row'} sm={2}>{editParam.label}</Label>
            <Col sm={10}>
              <DateSelectorContainer
                id={editParam.label}
                value={editingData[editParam.name]}
                onChange={() => { }}
              />
            </Col>
          </FormGroup>
        }
        return <FormGroup row>
          <Label for={'row'} sm={2}>{editParam.label}</Label>
          <Col sm={10}>
            <TextInput
              autoComplete={'off'}
              id={editParam.label}
              underlined
              value={editingData[editParam.name]}
            />
          </Col>
        </FormGroup>
      })
      return <div>
        {editors}
        <Button
          key='activatedInTable'
          color="primary"
          onClick={this.handleEdit}
        >
          {'enviar'}
        </Button>
      </div>
    }
    if (head.length === 1) {
      return list(data, head)
    }

    const activatedArr = _.map(data, (x, i) => {
      if (activated.includes(i)) {
        return {
          ...x,
          activated: true,
        }
      }
      return {
        ...x,
        activated: false,
      }
    })
    if (activatedArr.length === 0) {
      return <div>Esta tabla y/o seleccion aun no tiene datos</div>
    }
    const params = {
      data: activatedArr,
      editFn: this.editFn,
      extras,
      head,
      localState,
      filtered,
      name,
      title,
      switchEfect,
      toggleAll: this.toggleAll,
      toggleSingle: this.toggleSingle,
    }
    const containerForTable = filtered
      ? <FilterWraper
        {...params}
        ComponentName={TableContainer}
        dateRangeFilter={dateRangeFilter}
        dateRangeFilterKey={dateRangeFilterKey}
        filterKeys={filterKeys}

        key='FilterWraperForTable'
      />
      : <TableContainer
        {...params}
        key='Table'
      />

    const stage1 = (_checkboxFn && selector === 'all')
      ? [<Switch
        activated={this.state.activated.length}
        fn={this.toggleAll}
        key='switchAllTable'
        selector='single'
        text='select all'
        withText={true}
      >
      </Switch>]
      : []

    const stage2 = _.concat(stage1, [containerForTable])

    const ret = selector
      ? _.concat(stage2, [
        <Button
          key='activatedInTable'
          color="primary"
          onClick={this.handleSubmitSelected}
          disabled={this.state.submited}
        >
          {`${_checkboxFn.name} ${activated.length}`}
        </Button>,
      ])
      : stage2
    return ret
  }
}

export default Table
