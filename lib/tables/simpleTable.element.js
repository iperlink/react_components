import React from 'react'
import { Link } from 'react-router-dom'
import _ from 'lodash'
import { Column, Table, ColumnSizer } from 'react-virtualized'
import 'react-virtualized/styles.css'
import FaBeer from 'react-icons/lib/fa/beer'
import FaLocationArrow from 'react-icons/lib/fa/location-arrow'
import MinusCircle from 'react-icons/lib/fa/minus-circle'
import { Button } from 'reactstrap'
import Switch from '../elements/Switch'

const customCells = {
  _checkbox: ({
    obj,
    toggleSingle,
  }) => {
    const {
      rowData,
      rowIndex,
    } = obj
    return <Switch
      fn={toggleSingle}
      switchData={{ rowData, rowIndex }}
      activated={rowData.activated}
    ></Switch>
  },
  _edit: ({
    obj,
    editFn,
  }) => {
    const {
      rowData,
    } = obj
    return <Button onClick={() => { editFn(rowData) }} > <FaBeer /></Button>
  },
  _link: ({
    obj,
  }) => {
    const {
      rowData,
      dataKey,
    } = obj
    if (rowData[dataKey]) {
      return <a target="_blank" href={rowData[dataKey]}>
        <FaLocationArrow className={'linkArrow'} />
      </a>
    }
    return <MinusCircle className={'linkArrow'} />
  },
}

const getRowHeight = (data, { index }, head) => {
  const current = data[index]
  const params = _.omit(_.pick(current, _.keys(head)), ['url', 'images', 'videos', 'report', 'kml'])
  const numbers = _.map(_.values(params), x => {
    if (x) {
      return x.length
    }
    return 0
  })
  const max = Math.max(...numbers)
  const lines = Math.ceil(max / 12)
  const height = (lines === 1)
    ? 32
    : (lines < 8)
      ? lines * 24
      : lines * 10
  return height
}


export const simpleTable = props => {
  const {
    head,
    data,
    name,
    height,
    width,
    toggleSingle,
    editFn,
  } = props
  const keyedHead = _.keyBy(head, 'key')
  const ret = <Table
    id={name}
    headerHeight={20}
    rowHeight={index => getRowHeight(data, index, keyedHead)}
    width={width}
    height={height}
    rowCount={data.length}
    rowGetter={({ index }) => data[index]}
  >
    {
      head.map((x, i) => {
        const size = x.size || 100
        if (Object.keys(customCells).includes(x.key) || x.custom) {
          const renderer = x.custom || x.key
          return <Column
            label={x.title}
            dataKey={x.key}
            flexGrow={1}
            width={size}
            key={i}
            cellRenderer={obj => (
              customCells[renderer]({
                obj,
                toggleSingle,
                editFn,
              }))}
          />
        }

        return <Column
          label={x.title}
          className={`tableCell${x.key}`}
          flexGrow={1}
          width={size}
          dataKey={x.key}
          key={i}
        />
      })
    }
  </Table>
  return ret
}
