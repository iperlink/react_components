import React, { Component } from 'react'
import 'react-virtualized/styles.css'
import Select from 'react-virtualized-select'
import 'react-virtualized-select/styles.css'
import 'react-select/dist/react-select.css'

class SelectContainer extends Component {
  state = {
    selectedOption: 'sadas',
  }

  componentWillMount() {
    this.setState({
      selectedOption: this.props.value,
    })
  }

  handleChange = selectedOption => {
    this.setState({ selectedOption })
    const {
      formData,
      setFormData,
      onChange,
    } = this.props
    onChange && onChange(selectedOption.value, setFormData, formData)
  }

  render() {
    const {
      multi,
      loadOptions,
      onValueClick,
      async,
      mutate,
      options,
      formData,
      id,
    } = this.props

    if (async) {
      return (<Select
        async
        loadOptions={loadOptions}
        multi={multi}
        onChange={this.handleChange}
        onValueClick={v => onValueClick(v, mutate, formData)}
        value={this.state.selectedOption}
        />)
    }

    return (
      <Select
        multi={multi}
        options={options}
        id={id}
        onChange={this.handleChange}
        onValueClick={v => onValueClick(v, mutate, formData)}
        value={this.state.selectedOption}
        />
    )
  }
}

export default SelectContainer
