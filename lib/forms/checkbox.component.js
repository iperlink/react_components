import React, { Component } from 'react'
import _ from 'lodash'
import { Input, Label, FormGroup } from 'reactstrap'

import { toggleItemInArray } from '../utils'

const styles = {
  check: {
    position: 'relative',
    height: '25px',
    width: '25px',
    marginRight: '10px',
  },
}
class CheckboxContainer extends Component {
  state = {
    selectedOptions: '',
  }

  handleChange = selectedOption => {
    const {
      formData,
      setFormData,
      onChange,
    } = this.props
    const newSelected = toggleItemInArray(this.state.selectedOptions, selectedOption)
    this.setState({ selectedOptions: newSelected })
    onChange(newSelected.join(','), setFormData, formData)
  }

  render() {
    const {
      options,
    } = this.props
    const boxes = _.map(options, O => {
      const result = <FormGroup>
        <Input
          style={styles.check}
          type="checkbox"
          id="exampleCustomCheckbox"
          label="Check this custom checkbox"
          onChange={() => {this.handleChange(O.value)} }
          />
          <Label>{O.label}</Label>
        </FormGroup>
      return result
    })
    return (
      <div>
        {boxes}
      </div>

    )
  }
}

export default CheckboxContainer
