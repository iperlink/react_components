import React from 'react'
import { Input } from 'reactstrap'

const handleChange = (event, props) => {
  const {
    formData,
    setFormData,
    onChange,
  } = props
  const { target: { value } } = event
  onChange(value, setFormData, formData)
}
const DateSelectorContainer = props => {
  const {
    id,
  } = props
  const result = <Input
      type="time"
      id={id}
      onChange={e => { handleChange(e, props) }}
    />

  return result
}


export default DateSelectorContainer
