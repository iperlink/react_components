import React from 'react'
import _ from 'lodash'
import { TextField } from 'office-ui-fabric-react/lib/TextField';
import Select from './Select.component'
import {
  Button,
  FormGroup,
  Label,
  FormText,
  Col,
  Row,
} from 'reactstrap';


const inlineInput = props => {
  const {
    type,
    data,
    config,
    input,
    onChange,
    onBlur,
    mutate,
    formData
  } = this.props
  let ret
  const {
    placeholder,
    disabled,
    autoComplete,
    id,
    label
  } = config
  switch (type) {
    case 'text':
      return (
      <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
        <Label for={id} className="mr-sm-2">{label}</Label>
          <TextField
            underlined
            onChanged={v => onChange(v, mutate, formData)}
            onBlur={onBlur}
            id={id}
            disabled={disabled}
            placeholder= {placeholder}
            autoComplete={autoComplete}
        />
      </FormGroup>
    )
    case 'asyncSelect':
      return (
        <FormGroup className="mb-2 mr-sm-2 mb-sm-0">
          <Label for={id} className="mr-sm-2">{label}</Label>
          <Select
            multi={config.multi}
            value={config.value}
            async
            id={id}
            formData={formData}
            mutate={mutate}
            onChange={onChange}
            onValueClick={config.onValueClick}
            valueKey={config.valueKey}
            labelKey={config.labelKey}
            loadOptions={config.loadOptions}
          />
        </FormGroup>
        )
      case 'select':
        return (
          <Select
            options={config.options}
        />)
    } 
}

export default inlineInput