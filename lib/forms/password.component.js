import React from 'react'
import { Input } from 'reactstrap'

const handleChange = (event, props) => {
  const {
    formData,
    setFormData,
    onChange,
  } = props
  const { target: { value } } = event
  onChange(value, setFormData, formData)
}
const Password = props => {
  const {
    id,
  } = props
  const result = <Input
      type="password"
      id={id}
      onChange={e => { handleChange(e, props) }}
    />

  return result
}


export default Password
