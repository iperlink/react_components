import React, { Component } from 'react'
import { TextField } from 'office-ui-fabric-react/lib/TextField'

export default class TextInput extends Component {
  state = {
    value: false,
  }

  componentWillMount() {
    this.setState({
      value: this.props.value,
    })
  }

  handleChange = value => {
    const {
      onChange,
      setFormData,
      formData,
    } = this.props
    this.setState({
      value,
    })
    
    onChange && onChange(value, setFormData, formData)
  }

  render() {
    const {
      disabled,
      id,
      placeholder,
      setFormData,
    } = this.props

    const {
      value,
    } = this.state
    
    return (<TextField
            autoComplete={'new password'}
            disabled={disabled}
            id={id}
            value={value}
            onChanged={this.handleChange}
            placeholder= {placeholder}
            setFormData={setFormData}
            underlined
        />)
  }
}
