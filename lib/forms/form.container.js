import React from 'react'
import _ from 'lodash'
import Form from './Form'
import { getDataForField } from './form.utils'
import BreadcrumbComp from '../components/BreadcrumComp'

const FormContainer = (props, formWithSubmmit) => {
  const {
    args,
    formData,
    listArray,
    match,
    queryToUpdate,
    setFormData,
    omitMutate,
    handleSubmit,
    retry,
  } = props
  const {
    fields,
    handleSubmitFn,
    inline,
    paramToUpdate,
    className,
    title,
    logo,
    submitBtnTxt,
    submitBtnMsg,
    message,
  } = formWithSubmmit
  const fieldsWithData = getDataForField(fields, props)
  const handleSubmitFinal = handleSubmit || handleSubmitFn
  if (omitMutate) {
    const result = <Form
      fields={fieldsWithData}
      logo={logo}
      formData={formData}
      handleSubmitFn={handleSubmitFinal}
      retry={retry}
      className={className}
      setFormData={setFormData}
      submitBtnTxt={submitBtnTxt}
      submitBtnMsg={submitBtnMsg}
      message={message}
    />
    return result
  }
  const {
    formName,
  } = args
  const mutate = props[formName]
  if (!mutate) {
    console.error('mutate is not defined', 'props:', _.keys(props), formName)
  }
  const result = [
    <BreadcrumbComp
      title={title}
      args={args}
      match={match}
    />,
    <Form
      logo={logo}
      fields={fieldsWithData}
      formData={formData}
      handleSubmitFn={handleSubmitFinal}
      inline={inline}
      listArray={listArray}
      className={className}
      mutate={mutate}
      paramToUpdate={paramToUpdate}
      queryToUpdate={queryToUpdate}
      retry={retry}
      setFormData={setFormData}
      message={message}
    />,
  ]
  return result
}

export default FormContainer
