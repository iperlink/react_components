import React, { Component } from 'react'
import { Button } from 'reactstrap'
import { post } from 'axios'
import _ from 'lodash'
import Spinner from 'react-spinkit'

import FormCreator from './FormCreator'
const styles = {
  cont: {
    width: '100%',
    height: '100%',
  },
  spinner: {
    marginLeft: 'auto',
    marginRight: 'auto',
    color: 'white',
    width: '60px',
    height: '60px',
  }
}
const apiAdr = (process.env.NODE_ENV === 'production')
  ? 'https://api.wason.cloud/upload'
  : 'http://localhost:4000/upload'

export default class CreatedForm extends Component {
  state = {
    submited: false,
    message: '',
    result: false,
    file: false,
    values: {},
    uploading: false,
  }

  again = e => {
    e.preventDefault()
    this.setState({
      message: '',
      result: false,
    })
  }
  fileUpload = file => {
    const url = apiAdr
    const formData = new FormData()
    formData.append('file', file)
    const config = {
      headers: {
        'content-type': 'multipart/form-data',
      },
    }
    return post(url, formData, config)
  }
  handleSubmit = async e => {
    e.preventDefault()
    const {
      formData,
      handleSubmitFn,
      mutate,
      paramToUpdate,
      queryToUpdate,
      setFormData,
      fields,
      message,
    } = this.props
    const uploaders = _.filter(fields, { type: 'file' })
    if (uploaders.length) {
      this.setState({
        uploading: true,
      })
      const promises = _.map(uploaders, uploader => {
        const file = document.getElementById(uploader.config.id).files[0]
        return this.fileUpload(file)
      })
      await Promise.all(promises)
      this.setState({
        uploading: false,
        result: 'Cargando...',
      })
    }
    const result = await handleSubmitFn({
      mutate,
      formData,
      paramToUpdate,
      queryToUpdate,
      setFormData,
    })
    const Message = result
      ? message || 'Enviado con exito'
      : 'Ha ocurrido un error'
    this.setState({
      message: Message,
      result,
    })
  }

  render() {
    const {
      fields,
      formData,
      inline,
      mutate,
      setFormData,
      id,
      className,
      title,
      logo,
      submitBtnTxt,
      submitBtnMsg,
    } = this.props
    const {
      result,
      uploading,
      message,
    } = this.state

    const Fields = result
      ? <div className='sentmessage'>{message}</div>
      : uploading
        ? <div
          className={'spinnerCont'}
          style={styles.cont}
        >
          <Spinner
            name='double-bounce'
            style={styles.spinner}
          />
          <div className='sentmessage'>Cargando archivos</div>
        </div>
        : FormCreator({
          fields,
          formData,
          inline,
          mutate,
          setFormData,
        })
    const cl = className || 'defaultForm'
    const logoDiv = logo
      ? <div className={'formLogo'} />
      : null
    const buttonFn = result
      ? this.again
      : this.handleSubmit
    const btnText = result
      ? 'Crear mas'
      : submitBtnTxt || 'Enviar'
    if (result && submitBtnMsg === 'false') {
      return <form
        key={id}
        inline={inline}
        className={cl}
        id={id}
        onSubmit={buttonFn}
      >
        <div>{logoDiv}</div>
        <span>{title}</span>
        {Fields}
        <div className='formBtnCont'>
        </div>
      </form>
    }
    return <form
      key={id}
      inline={inline}
      className={cl}
      id={id}
      onSubmit={buttonFn}
    >
      <div>{logoDiv}</div>
      <span>{title}</span>
      {Fields}
      <div className='formBtnCont'>
        <Button
          type="submit"
        >{btnText}</Button>
      </div>
    </form>
  }
}
