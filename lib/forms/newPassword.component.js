import React from 'react'
import { Input } from 'reactstrap'

const handleChange = (event, props) => {
  const {
    formData,
    setFormData,
    onChange,
  } = props
  const { target: { value } } = event
  onChange(value, setFormData, formData)
}
const NewPassword = props => {
  const result = <Input
      type="password"
      onChange={e => { handleChange(e, props) }}
    />

  return result
}


export default NewPassword
