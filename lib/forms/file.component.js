import React from 'react'
import { Input } from 'reactstrap'

const handleChange = (event, props) => {
  const {
    formData,
    setFormData,
    onChange,
  } = props
  const { target: { files } } = event

  onChange(files[0].name, setFormData, formData, true)
}
const DateSelectorContainer = props => {
  const {
    id,
  } = props
  const result = <Input
    type="file"
    id={id}
    onChange={e => { handleChange(e, props) }}
  />

  return result
}


export default DateSelectorContainer
