import React from 'react'
import _ from 'lodash'

import Input from './input.hoc'
import InlineInput from './inlineInput.hoc'

const FormCreator = props => {
  const {
    fields,
    formData,
    inline,
    mutate,
    setFormData,
  } = props
  const fieldsMapped = inline
    ? _.map(fields, field => {
      const F = <InlineInput
        {...field}
        formData={formData}
        key={field.name}
        mutate={mutate}
        setFormData={setFormData}
      />
      return F
    })
    : _.map(fields, field => {
      const F = <Input
        {...field}
        formData={formData}
        key={field.name}
        mutate={mutate}
        setFormData={setFormData}
      />
      return F
    })
  return fieldsMapped
}

export default FormCreator
