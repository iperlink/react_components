import _ from 'lodash'

export const getDataForField = (fields, P) => {
  const withData = _.map(fields, field => {
    const { config } = field
    if (config.dataFrom) {
      const {
        Q,
        label,
        value = '_id',
      } = config.dataFrom

      const out = P[Q]
      if (!out) {
        console.error(`${Q} is not defined in `, _.keys(_.omit(P, [
          'match',
          'location',
          'history',
          'staticContext',
          'components',
          'dataSources',
          'flexArray',
          'args'])))
      }
      const data = out[Q]
      const options = _.map(data, d => {
        const result = { label: d[label], value: d[value] }
        return result
      })
      const configNew = {
        ...config,
        options,
      }
      const result = {
        ...field,
        config: configNew,
        options,
        data,
      }
      return result
    }
    return field
  })
  return _.compact(withData)
}
