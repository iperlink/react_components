import React, { Component } from 'react'
import { Input } from 'reactstrap'
import _ from 'lodash'


class DateSelectorContainer extends Component {
  state = {
    selectedOption: null,
  }
  componentWillMount() {
    if (this.props.value) {
      const value = _.head(this.props.value.split('T'))
      this.setState({
        selectedOption: value,
      })
    }
  }
  handleChange = event => {
    const { target: { value } } = event
    this.setState({ selectedOption: value })
    const {
      formData,
      setFormData,
      onChange,
    } = this.props
    onChange && onChange(value, setFormData, formData)
  }
  render() {
    const {
      id,
    } = this.props
    const result = <Input
      type="date"
      id={id}
      value={this.state.selectedOption}
      onChange={e => { this.handleChange(e) }}
    />

    return result
  }
}


export default DateSelectorContainer
