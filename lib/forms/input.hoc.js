import React from 'react'
import {
  Col,
  FormGroup,
  Label,
} from 'reactstrap'

import TextInput from './textInput.component'
import Select from './Select.component'
import DateSelectorContainer from './dateSelector.component'
import TimeSelectorContainer from './timeSelector.component'
import Checkbox from './checkbox.component'
import Password from './password.component'
import File from './file.component'

const input = props => {
  const {
    config,
    formData,
    mutate,
    onChange,
    onBlur,
    setFormData,
    type,
  } = props
  const {
    disabled,
    id,
    label,
    labelKey,
    multi,
    loadOptions,
    onValueClick,
    options,
    placeholder,
    valueKey,
    defaultValue,
  } = config
  switch (type) {
    case 'text':
      return (
      <FormGroup row>
        <Label for={id} sm={2}>{label}</Label>
        <Col sm={10}>
          <TextInput
            autoComplete={'off'}
            formData={formData}
            disabled={disabled}
            id={id}
            onBlur={onBlur}
            onChange={onChange}
            
            setFormData={setFormData}
            underlined
            value={defaultValue}
        />
      </Col>
      </FormGroup>
      )
    case 'file':
      return (
      <FormGroup row>
        <Label for={id} sm={2}>{label}</Label>
        <Col sm={10}>
          <File
            id={id}
            onBlur={onBlur}
            formData={formData}
            onChange={onChange}
            placeholder= {placeholder}
            setFormData={setFormData}
            underlined
            value={defaultValue}
        />
      </Col>
      </FormGroup>
      )
    case 'password':
      return (
      <FormGroup row>
        <Label for={id} sm={2}>{label}</Label>
        <Col sm={10}>
          <Password
              formData={formData}
              id={id}
              onChange={onChange}
              mutate={mutate}
              setFormData={setFormData}
            />
      </Col>
      </FormGroup>
      )

    case 'asyncSelect':
      return (
        <FormGroup row>
          <Label for={id} sm={2}>{label}</Label>
          <Col sm={10}>
          <Select
            async
            formData={formData}
            id={id}
            labelKey={labelKey}
            loadOptions={loadOptions}
            multi={multi}
            mutate={mutate}
            onChange={onChange}
            onValueClick={onValueClick}
            setFormData={setFormData}
            value={'Semanal'}
            valueKey={defaultValue}
          />
        </Col>
        </FormGroup>
      )
    case 'select':
      return (
          <FormGroup row>
            <Label for={id} sm={2}>{label}</Label>
            <Col sm={10}>
            <Select
              formData={formData}
              id={id}
              labelKey={labelKey}
              onChange={onChange}
              onValueClick={onValueClick}
              options={options}
              mutate={mutate}
              setFormData={setFormData}
              valueKey={valueKey}
              value={defaultValue}
            />
            </Col>
        </FormGroup>)
    case 'checkbox':
      return (
          <FormGroup row>
            <Label for={id} sm={2}>{label}</Label>
            <Col sm={10}>
            <Checkbox
              formData={formData}
              id={id}
              onChange={onChange}
              options={options}
              setFormData={setFormData}
              value={defaultValue}
            />
            </Col>
        </FormGroup>)
    case 'date':
      return (
          <FormGroup row>
            <Label for={id} sm={2}>{label}</Label>
            <Col sm={10}>
            <DateSelectorContainer
              formData={formData}
              id={id}
              onChange={onChange}
              mutate={mutate}
              setFormData={setFormData}
            />
            </Col>
          </FormGroup>
      )
    case 'time':
      return (
        <FormGroup row>
        <Label for={id} sm={2}>{label}</Label>
        <Col sm={10}>
        <TimeSelectorContainer
              formData={formData}
              id={id}
              onChange={onChange}
              mutate={mutate}
              setFormData={setFormData}
            />
        </Col>
      </FormGroup>
      )
    default:
      return null
  }
}

export default input
