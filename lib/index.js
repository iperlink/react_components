import AppContainer from './app/AppContainer.hoc'
import { Button } from 'reactstrap'
import CardRow from './wistia/CardRow.hoc'
import Flex from './hoc/Flex'
import Form from './forms/form.container'
import Loader from './hoc/Loader'
import Table from './tables/Table.hoc'
import Wistia from './wistia/Wistia.component'
import GalleryRow from './gallery/gallery.component'
import Kml from './kml/kml.component'
//import Login from './app/login'
import Pdf from './pdf/pdf'

const Tester = Table
module.exports = {
  AppContainer,
  Button,
  CardRow,
  Form,
  GalleryRow,
  Kml,
  //Login,
  Loader,
  Flex,
  Pdf,
  Table,
  Tester,
  Wistia,
}
